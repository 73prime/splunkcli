.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean, showcoverage, tests, build, containertests, or run" with make, por favor.

showcoverage: tests
	@echo Running Coverage output
	go tool cover -html=coverage.out

containertests:
	@docker-compose -f docker-compose.yml up --build --abort-on-container-exit
	@docker-compose -f docker-compose.yml down --volumes

containertestexecution: clean
	@echo Running Tests
	/app/wait_for_endpoint.sh -t 180 https://splunk:8089
	sleep 5
	go test --coverprofile=coverage.out ./...

tests: clean
	@echo Running Tests
	go test --coverprofile=coverage.out ./...

run: build
	@echo Running program
	RUN_LOCAL=true ./bin/splunkcli

build:
	@echo Running build command
	go build -o bin/splunkcli ./src

clean:
	@echo Removing binary
	rm -rf ./bin ./vendor Gopkg.lock
	rm -rf coverage.out

deploy: clean build
	sls deploy --verbose