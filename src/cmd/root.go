/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"errors"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"log"
	"os"
	"strings"
)

var cfgFile string
var (
	SplunkURL, SplunkUser, SplunkPassword, SplunkDirectory string
)

var rootCmd = &cobra.Command{
	Use:   "splunkcli",
	Short: "This interfaces with Splunk REST API.",
	Long: `This is the fabulous Splunkcli tool.
Generally, you use it to do CRUD operations on 
a Splunk instance.`,
	//Run: func(cmd *cobra.Command, args []string) {
	//	//to get here, the command is executed without switches....
	//},
}
var getCmd = &cobra.Command{
	Use:   "get",
	Short: "Get verb for splunk things (apps, views, alerts)",
	Long:  `This interfaces with the Splunk REST API`,
}
var exportCmd = &cobra.Command{
	Use:   "export",
	Short: "Export verb for splunk things (views, alerts)",
	Long:  `This interfaces with the Splunk REST API`,
}
var syncCmd = &cobra.Command{
	Use:   "sync",
	Short: "Sync verb for splunk things (views, alerts)",
	Long:  `This interfaces with the Splunk REST API`,
}
var deleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete verb for splunk things (views, alerts)",
	Long:  `This interfaces with the Splunk REST API`,
}
var updateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update verb for splunk things (views, alerts)",
	Long:  `This interfaces with the Splunk REST API`,
}
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "Create verb for splunk things (views, alerts)",
	Long:  `This interfaces with the Splunk REST API`,
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

//infile is STDIN, and used at this point to mock. Pass nil in on this for regular usage.
func Ask4confirm(message string, stdinfile *os.File) bool {
	if stdinfile == nil {
		stdinfile = os.Stdin
	}
	if !viper.GetBool("auto-approve") {
		var s string
		fmt.Printf("%s (y/N): ", message)

		//_, err := fmt.Scan(&s)
		_, err := fmt.Fscanf(stdinfile, "%s", &s)
		if err != nil {
			return false
		}
		s = strings.TrimSpace(s)
		s = strings.ToLower(s)

		if s == "y" || s == "yes" {
			return true
		}
		return false
	} else {
		return true
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.splunkcli.yaml)")
	rootCmd.PersistentFlags().Bool("auto-approve", false, "Approve without Y/N")

	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	viper.BindPFlag("auto-approve", rootCmd.PersistentFlags().Lookup("auto-approve"))

	rootCmd.PersistentFlags().String(
		"SPLUNK_URL",
		"",
		"URL For Splunk, with https:, and port")
	rootCmd.PersistentFlags().String(
		"SPLUNK_USER",
		"",
		"User to use for API calls to Splunk")
	rootCmd.PersistentFlags().String(
		"SPLUNK_PASSWORD",
		"",
		"Password to use for API calls to Splunk")
	rootCmd.PersistentFlags().String(
		"SPLUNK_DIRECTORY",
		"",
		"Directory to use for Splunk things")

	viper.BindPFlag("SPLUNK_URL", rootCmd.PersistentFlags().Lookup("SPLUNK_URL"))
	viper.BindPFlag("SPLUNK_USER", rootCmd.PersistentFlags().Lookup("SPLUNK_USER"))
	viper.BindPFlag("SPLUNK_PASSWORD", rootCmd.PersistentFlags().Lookup("SPLUNK_PASSWORD"))
	viper.BindPFlag("SPLUNK_DIRECTORY", rootCmd.PersistentFlags().Lookup("SPLUNK_DIRECTORY"))
	rootCmd.AddCommand(getCmd)
	rootCmd.AddCommand(exportCmd)
	rootCmd.AddCommand(syncCmd)
	rootCmd.AddCommand(deleteCmd)
	rootCmd.AddCommand(updateCmd)
	rootCmd.AddCommand(createCmd)
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)
		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".splunkcli")
	}
	viper.AutomaticEnv() // read in environment variables that match
	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
	collectFlags()
}
func collectFlags() {
	SplunkURL = viper.GetString("SPLUNK_URL")
	SplunkUser = viper.GetString("SPLUNK_USER")
	SplunkPassword = viper.GetString("SPLUNK_PASSWORD")
	SplunkDirectory = viper.GetString("SPLUNK_DIRECTORY")
	if SplunkURL == "" || SplunkUser == "" || SplunkPassword == "" {
		log.Panic("You must set SPLUNK_URL, SPLUNK_USER, SPLUNK_PASSWORD somewhere, either flags, envvars, or in the config file")
	}
}
func exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}
func writeToDirectory(data string, objType string, filename string) bool {
	if SplunkDirectory == "" || data == "" || filename == "" || objType == "" {
		return false
	}
	os.Mkdir(SplunkDirectory, os.ModePerm)
	os.Mkdir(fmt.Sprintf("%s/%s", SplunkDirectory, objType), os.ModePerm)
	fullFile := fmt.Sprintf("%s/%s/%s", SplunkDirectory, objType, filename)
	err1 := os.Remove(fullFile)
	if err1 != nil {
		fmt.Printf(err1.Error())
	}
	if err := os.WriteFile(fullFile, []byte(data), 0666); err != nil {
		log.Fatal(err)
	}
	return true
}
