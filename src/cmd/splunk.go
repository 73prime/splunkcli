package cmd

import (
	"bytes"
	"crypto/tls"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)


/*  this interface/implementation is to facilitate mocking/testing */
type SplunkWebInterface interface {
	HttpProcessor(verb, url, username, password string, payload []byte) ([]byte, error)
}
var SplunkClient SplunkWebInterface

type splunkWebImpl struct{}
func (*splunkWebImpl) HttpProcessor(verb, url, username, password string, payload []byte) ([]byte, error) {
	fmt.Printf("Using URL: %s, with verb: %s\n", url, verb)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	req, _ := http.NewRequest(verb, url, bytes.NewBuffer(payload))
	req.SetBasicAuth(username, password)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err.Error())
		return nil, err
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >=400 {
		err = errors.New(string(body))
	}
	return body, err
}
