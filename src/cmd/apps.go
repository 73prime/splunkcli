/*
Copyright © 2021, 73 Prime LLC; Allen Plummer; allen@73prime.io

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"encoding/xml"
	"fmt"
	"github.com/spf13/cobra"
	"net/url"
)

type Apps struct {
	XMLName    xml.Name   `xml:"feed"`
	AppEntries []AppEntry `xml:"entry"`
}
type AppEntry struct {
	XMLName xml.Name `xml:"entry"`
	Title   string   `xml:"title"`
}

var GetSplunkAppsCmd = &cobra.Command{
	Use:   "apps",
	Short: "Gets list of Splunk Apps",
	Long:  `This interfaces with the Splunk REST API, and gets list of apps.`,
	Run: func(cmd *cobra.Command, args []string) {
		GetSplunkApps()
	},
}
var CreateSplunkAppCmd = &cobra.Command{
	Use:   "app",
	Short: "Creates Splunk App",
	Long:  `This interfaces with the Splunk REST API, and gets list of apps.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			panic("You must have an app name to use here.")
		}
		CreateSplunkApp(args[0])
	},
}
var DeleteSplunkAppCmd = &cobra.Command{
	Use:   "app",
	Short: "Deletes Splunk App",
	Long:  `This interfaces with the Splunk REST API, and gets list of apps.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) != 1 {
			panic("You must have an app name to use here.")
		}
		fmt.Printf("TODO: This is intentionally not completed yet.")
	},
}

func GetSplunkApps() error {
	//follows the pattern: curl -ku user:password https://localhost:8089/services/apps/local/
	url := fmt.Sprintf("%s/services/apps/local", SplunkURL)
	data, err := SplunkClient.HttpProcessor("GET", url, SplunkUser, SplunkPassword, nil)
	if err != nil {
		fmt.Println(err)
		return err
	}
	_, err = ParseSplunkApps(data)
	return err
}
func CreateSplunkApp(appname string) error {
	params := url.Values{}
	params.Add("name", appname)

	url := fmt.Sprintf("%s/services/apps/local", SplunkURL)
	_, err := SplunkClient.HttpProcessor("POST", url, SplunkUser, SplunkPassword, []byte(params.Encode()))
	return err
}
func DeleteSplunkApp(appname string) error {
	params := url.Values{}
	params.Add("name", appname)
	url := fmt.Sprintf("%s/services/apps/local/%s", SplunkURL, appname)
	_, err := SplunkClient.HttpProcessor("DELETE", url, SplunkUser, SplunkPassword, nil)
	return err
}
func ParseSplunkApps(appsXML []byte) (Apps, error) {
	var apps Apps
	err := xml.Unmarshal(appsXML, &apps)
	if err == nil {
		for idx, value := range apps.AppEntries {
			fmt.Printf("Splunk App #%d: %s\n", idx+1, value.Title)
		}
	} else {
		fmt.Printf("Error: %s", err.Error())
	}
	return apps, err
}

func init() {
	getCmd.AddCommand(GetSplunkAppsCmd)
	createCmd.AddCommand(CreateSplunkAppCmd)
	deleteCmd.AddCommand(DeleteSplunkAppCmd)
	SplunkClient = &splunkWebImpl{}
}
